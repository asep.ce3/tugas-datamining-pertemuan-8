/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataminingp8;

/**
 *
 * @author cahyana
 */
public class Udara {
    private String tgl;
    private String lokasi;
    private int pm10;
    private int so2;
    private int co;
    private int o3;
    private int no3;
    private String kategori;

    public Udara(String tgl, String lokasi, int pm10, int so2, int co, int o3, int no3, String kategori) {
        this.tgl = tgl;
        this.lokasi = lokasi;
        this.pm10 = pm10;
        this.so2 = so2;
        this.co = co;
        this.o3 = o3;
        this.no3 = no3;
        this.kategori = kategori;
    }

    public Udara() {
    }
    
    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public int getPm10() {
        return pm10;
    }

    public void setPm10(int pm10) {
        this.pm10 = pm10;
    }

    public int getSo2() {
        return so2;
    }

    public void setSo2(int so2) {
        this.so2 = so2;
    }

    public int getCo() {
        return co;
    }

    public void setCo(int co) {
        this.co = co;
    }

    public int getO3() {
        return o3;
    }

    public void setO3(int o3) {
        this.o3 = o3;
    }

    public int getNo3() {
        return no3;
    }

    public void setNo3(int no3) {
        this.no3 = no3;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    @Override
    public String toString() {
        return "Udara{" + "tgl=" + tgl + ", lokasi=" + lokasi + ", pm10=" + pm10 + ", so2=" + so2 + ", co=" + co + ", o3=" + o3 + ", no3=" + no3 + ", kategori=" + kategori + '}';
    }
    
    public boolean isSedang() {
        return getKategori().toUpperCase().equals("SEDANG");
    }
    
    public boolean isBaik() {
        return getKategori().toUpperCase().equals("BAIK");
    }
    
    public boolean isTidakBaik() {
        return getKategori().toUpperCase().equals("TIDAK SEHAT");
    }
}
