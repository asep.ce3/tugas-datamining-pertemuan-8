
package dataminingp8;

public class Result {
    private String kategori;
    private double average;
    private double deviasiStd;

    public Result(String kategori, double average, double deviasiStd) {
        this.kategori = kategori;
        this.average = average;
        this.deviasiStd = deviasiStd;
    }

    public Result() {
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
    
    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getDeviasiStd() {
        return deviasiStd;
    }

    public void setDeviasiStd(double deviasiStd) {
        this.deviasiStd = deviasiStd;
    }
}
