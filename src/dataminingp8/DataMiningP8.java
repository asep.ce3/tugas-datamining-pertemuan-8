
package dataminingp8;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataMiningP8 {

    public static void main(String[] args) {
        List<Udara> data = new ArrayList();
        FileReader reader = null;
        try {
            File file = new File("data.txt");
            reader = new FileReader(file);
            char[] buf = new char[1024];
            Scanner scanner = new Scanner(reader);
            
            while ( scanner.hasNext()) {
                String temp = scanner.nextLine();
                StringTokenizer token = new StringTokenizer(temp, "\t");
                Udara udara = new Udara(
                        token.nextToken(),
                        token.nextToken(),
                        Integer.parseInt(token.nextToken()),
                        Integer.parseInt(token.nextToken()),
                        Integer.parseInt(token.nextToken()),
                        Integer.parseInt(token.nextToken()),
                        Integer.parseInt(token.nextToken()),
                        token.nextToken()
                );
                data.add(udara);
            }
        } catch (IOException ex) {
            Logger.getLogger(DataMiningP8.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (reader != null) reader.close();
            } catch (IOException ex) {
                Logger.getLogger(DataMiningP8.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (!data.isEmpty()) {
            int jumlahBaik = 0;
            int jumlahSedang = 0;
            int jumlahTidakBaik = 0;
            for (Udara u : data) {
                if (u.isBaik()) {
                    jumlahBaik++;
                } else if (u.isSedang()) {
                    jumlahSedang++;
                } else if (u.isTidakBaik()) {
                    jumlahTidakBaik++;
                }
            }
            
            List<Result> resultPM10 = new ArrayList<>();
            List<Result> resultSO2 = new ArrayList<>();
            List<Result> resultCO = new ArrayList<>();
            List<Result> resultO3 = new ArrayList<>();
            List<Result> resultNO3 = new ArrayList<>();
            
            int[][] itemBaik = new int[5][jumlahBaik];
            for (int i=0; i<jumlahBaik; i++) {
                itemBaik[0][i] = data.get(i).getPm10();
                itemBaik[1][i] = data.get(i).getSo2();
                itemBaik[2][i] = data.get(i).getCo();
                itemBaik[3][i] = data.get(i).getO3();
                itemBaik[4][i] = data.get(i).getNo3();
            }
            resultPM10.add(cetakDeviasiStandar(itemBaik[0], "BAIK"));
            resultSO2.add(cetakDeviasiStandar(itemBaik[1], "BAIK"));
            resultCO.add(cetakDeviasiStandar(itemBaik[2], "BAIK"));
            resultO3.add(cetakDeviasiStandar(itemBaik[3], "BAIK"));
            resultNO3.add(cetakDeviasiStandar(itemBaik[4], "BAIK"));
            
            int[][] itemBaik2 = new int[5][jumlahSedang];
            for (int i=0; i<jumlahSedang; i++) {
                itemBaik2[0][i] = data.get(i).getPm10();
                itemBaik2[1][i] = data.get(i).getSo2();
                itemBaik2[2][i] = data.get(i).getCo();
                itemBaik2[3][i] = data.get(i).getO3();
                itemBaik2[4][i] = data.get(i).getNo3();
            }
            resultPM10.add(cetakDeviasiStandar(itemBaik2[0], "SEDANG"));
            resultSO2.add(cetakDeviasiStandar(itemBaik2[1], "SEDANG"));
            resultCO.add(cetakDeviasiStandar(itemBaik2[2], "SEDANG"));
            resultO3.add(cetakDeviasiStandar(itemBaik2[3], "SEDANG"));
            resultNO3.add(cetakDeviasiStandar(itemBaik2[4], "SEDANG"));
            
            int[][] itemBaik3 = new int[5][jumlahTidakBaik];
            for (int i=0; i<jumlahTidakBaik; i++) {
                itemBaik3[0][i] = data.get(i).getPm10();
                itemBaik3[1][i] = data.get(i).getSo2();
                itemBaik3[2][i] = data.get(i).getCo();
                itemBaik3[3][i] = data.get(i).getO3();
                itemBaik3[4][i] = data.get(i).getNo3();
            }
            resultPM10.add(cetakDeviasiStandar(itemBaik3[0], "TIDAK BAIK"));
            resultSO2.add(cetakDeviasiStandar(itemBaik3[1], "TIDAK BAIK"));
            resultCO.add(cetakDeviasiStandar(itemBaik3[2], "TIDAK BAIK"));
            resultO3.add(cetakDeviasiStandar(itemBaik3[3], "TIDAK BAIK"));
            resultNO3.add(cetakDeviasiStandar(itemBaik3[4], "TIDAK BAIK"));
            
            System.out.println("\nPM10");
            for (Result r : resultPM10) {
                System.out.print("- " + r.getKategori() + " [");
                System.out.print("Mean : " + r.getAverage());
                System.out.println(" Deviasi Standar : " + r.getDeviasiStd() + " ]");
            }
            
            System.out.println("\nSO2");
            for (Result r : resultSO2) {
                System.out.print("- " + r.getKategori() + " [");
                System.out.print("Mean : " + r.getAverage());
                System.out.println(" Deviasi Standar : " + r.getDeviasiStd() + " ]");
            }
            
            System.out.println("\nCO");
            for (Result r : resultCO) {
                System.out.print("- " + r.getKategori() + " [");
                System.out.print("Mean : " + r.getAverage());
                System.out.println(" Deviasi Standar : " + r.getDeviasiStd() + " ]");
            }
            
            System.out.println("\nO3");
            for (Result r : resultO3) {
                System.out.print("- " + r.getKategori() + " [");
                System.out.print("Mean : " + r.getAverage());
                System.out.println(" Deviasi Standar : " + r.getDeviasiStd() + " ]");
            }
            
            System.out.println("\nNO3");
            for (Result r : resultNO3) {
                System.out.print("- " + r.getKategori() + " [");
                System.out.print("Mean : " + r.getAverage());
                System.out.println(" Deviasi Standar : " + r.getDeviasiStd() + " ]");
            }
            
        }
    }
    
    private static Result cetakDeviasiStandar(int[] data, String kategori) {
        if (data.length == 0) return new Result();
        double count = 0;
        for (int i : data) {
            count += i;
        }
        double average = count / data.length;

        double sumStandar = 0;
        for (int i : data) {
            sumStandar += Math.pow(i - average, 2);
        }

        double deviasiStandar = Math.sqrt(sumStandar / (data.length-1));

        //System.out.println(kategori + ": count: " + count + "/" + data.length + ", mean: " + average + ", ss: " + sumStandar + ", dev: " + deviasiStandar);
        //return new Result(count + "/" + data.length + " s:" + sumStandar + " " + kategori, average, deviasiStandar);
        return new Result(kategori, average, deviasiStandar);
    }
}
